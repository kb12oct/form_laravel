
<!DOCTYPE html>
<html lang="en">

<head>
  <title>@section('title','list')</title>
</head>

<body>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Simple Table</h4>
                  <p class="card-category"> Here is a subtitle for this table</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                      <tr>
                            <th>id</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>Aboute</th>
                               
                            </tr>
                                                </thead>
                      <tbody>
                    @foreach($users as $list)
                        <tr>
                            <td>{{$list->id}}</td>
                            <td>{{$list->Username}}</td>
                            <td>{{$list->Email}}</td>
                            <td>{{$list->First_Name}}</td>
                            <td>{{$list->Last_Name}}</td>
                            <td>{{$list->Address}}</td>
                            <td>{{$list->City}}</td>
                            <td>{{$list->Aboute}}</td>
                          </tr>
                        @endforeach
 </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
           
    </div>
  </div>
    </div>
  </div>
  <script src="../assets/demo/demo.js"></script>
  
</body>
          
</body>
</html>