<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


route::view('list','livewire.list');
Route::get('user', \App\Http\Livewire\data::class);

route::view('form','livewire.forms');
Route::get('forms', \App\Http\Livewire\form::class);


