
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
      
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <link rel="{{asset('assets/stylesheet')}}" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="{{asset('assets/stylesheet')}}" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <link href="{{asset('assets/demo/demo.css')}}" rel="stylesheet" /></head>
  <link href="bootstrap.min.css.map" rel="stylesheet" /></head>
  <link href=" bootstrap.min.css" rel="stylesheet" /></head>
  
<body>
@livewireStyles
@livewire('header')
@livewire('users')
@livewire('footer')
@livewireScripts

  <script src="{{asset('assets/assets/js/core/jquery.min.js')}}"></script>
  <script src="{{asset('assets/assets/js/core/popper.min.js')}}"></script>
  <script src="{{asset('assets/assets/js/core/bootstrap-material-design.min.js')}}"></script>
  
  <script src="{{asset('assete/https:/maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE'))}"></script>
  <script src="{{asset('assets/assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
  <script src="bootstrap.min.js"></script>
  <script src="popper.min.js"></script>
  
  
  </body>

</html>
